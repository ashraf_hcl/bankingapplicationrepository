package com.student.dto;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Snippet {
	
	public static void main(String[] args) {
		
	   String[] logs = new String[] {
	
	            "2012-09-13 16:04:22 DEBUG SID:34523 BID:1329 RID:65d33 'Starting new session'",
	            "2012-09-13 16:04:30 DEBUG SID:34523 BID:1329 RID:54f22 'Authenticating User'",
	            "2012-09-13 16:05:30 DEBUG SID:42111 BID:319 RID:65a23 'Starting new session'",
	            "2012-09-13 16:04:50 ERROR SID:34523 BID:1329 RID:54ff3 'Missing Authentication token'",
	            "2012-09-13 16:05:31 DEBUG SID:42111 BID:319 RID:86472 'Authenticating User'",
	            "2012-09-13 16:05:31 DEBUG SID:42111 BID:319 RID:7a323 'Deleting asset with ID 543234'",
	            "2012-09-13 16:05:32 WARN SID:42111 BID:319 RID:7a323 'Invalid asset ID'" };
	
	    List<String> logList = new ArrayList<String>();
	    
	    for (String log : logs) {
	        logList.add(log);
	    }
	
	    
	    String fileName = "C:\\Users\\ashrafali.malagi\\Desktop\\Training Details\\Java8\\Logs\\Logger.txt";
		
		String[] logs1 =null;
		
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

		logs1 = stream.toArray(String[]::new);
			
			System.out.println("------------->"+logs1.length);
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		 for (String log : logs1) {
		        logList.add(log);
		    }
		
	    
	    Calendar cal = Calendar.getInstance();
		/*
		 * cal.set(Calendar.YEAR, 2012); cal.set(Calendar.MONTH, 8);
		 * cal.set(Calendar.DATE, 13); cal.set(Calendar.HOUR_OF_DAY, 16);
		 * cal.set(Calendar.MINUTE, 4); cal.set(Calendar.SECOND, 30);
		 * cal.set(Calendar.MILLISECOND, 0);
		 */
	    
	    cal.set(Calendar.YEAR, 2020);
	    cal.set(Calendar.MONTH, 9);
	    cal.set(Calendar.DATE, 25);
	    cal.set(Calendar.HOUR_OF_DAY, 03);
	    cal.set(Calendar.MINUTE, 27);
	    cal.set(Calendar.SECOND, 30);
	    cal.set(Calendar.MILLISECOND, 0);
	
	    Date startDate = cal.getTime();
	
	    cal.set(Calendar.MINUTE, 5);
	    cal.set(Calendar.SECOND, 31);
	
	    Date endDate = cal.getTime();
	
	    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	    Pattern pattern = Pattern.compile("^(0?[1-9]|[12][0-9]|3[01])-(0?[1-9]|1[012])-([12][0-9]{3})$");
	System.out.println("before matcher");
	    Matcher matcher = null;
	    String logString = null;
	    Date date = null;
	    for (int i = 0; i < logList.size(); i++) {
	        logString = logList.get(i);
	        matcher = pattern.matcher(logString);
	
	       // if (matcher.find()) {
	            try {
	                date = format.parse(matcher.group());
	                if (date.getTime() >= startDate.getTime()
	                        && date.getTime() <= endDate.getTime()) {
	                    System.out.println(logString);
	                }
	            } catch (ParseException e) {
	                e.printStackTrace();
	            }
	       // }
	
	    }
}

}