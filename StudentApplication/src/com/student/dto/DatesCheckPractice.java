



package com.student.dto;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class DatesCheckPractice {

	public static void main(String[] args) throws IOException, ParseException {

		// public static void main(String[] args) {
		/*
		 * String[] logs = new String[] {
		 * 
		 * "2012-09-13 16:04:22 DEBUG VISITID:34523 BID:1329 RID:65d33 'Starting new session'"
		 * ,
		 * "2012-09-13 12:04:30 DEBUG VISITID:34823 BID:1329 RID:54f22 'Authenticating User'"
		 * , "2012-09-13 16:05:30 DEBUG VISITID:42101 BID:319 RID:65a23 'Valid Users'",
		 * "2012-09-13 16:04:50 ERROR VISITID:34583 BID:1329 RID:54ff3 'Missing Authentication token'"
		 * ,
		 * "2012-09-26 16:05:31 DEBUG VISITID:42171 BID:319 RID:86472 'Granted Access to  User'"
		 * ,
		 * "2012-09-13 16:05:31 DEBUG VISITID:42118 BID:319 RID:7a323 'Deleting asset with ID 543234'"
		 * ,
		 * "2017-09-13 16:05:31 DEBUG VISITID:42011 BID:319 RID:7a323 'Updating asset with ID 543234'"
		 * ,
		 * "2012-09-13 15:04:50 ERROR VISITID:34623 BID:1329 RID:54ff3 'Missing Authentication token'"
		 * ,
		 * "2012-09-13 16:05:32 WARN  VISITID:42118 BID:319 RID:7a323 'Invalid asset ID'"
		 * };
		 */

		/*
		 * String file =
		 * "C:\\Users\\ashrafali.malagi\\Desktop\\Training Details\\Java8\\Logger.txt";
		 * List<String> logList = new ArrayList<String>(); BufferedReader reader = new
		 * BufferedReader(new FileReader(file)); String line = reader.readLine(); while
		 * (line != null) { logList.add(line); // read next line line =
		 * reader.readLine(); } reader.close();
		 */
		
String fileName = "C://Users//ashrafali.malagi//Desktop//Training Details//Java8//Logs//Logger.txt";
		
String[] logs1 =null;
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

		 logs1 = stream.toArray(String[]::new);
			System.out.println("--->"+logs1.length);
		} catch (IOException e) {
			e.printStackTrace();
		}

		for(int i=0;i<logs1.length;i++) {
			
			System.out.println("  sjsjjjj "+i+"---------"+logs1[i]);
		}
		
		List<String> logList = new ArrayList<String>();
		LocalDateTime startDt = null;
		Date startDateC = null;
		LocalDateTime endDt = null;

		Date eC = null;

		for (String log : logs1) {
			logList.add(log);
		}
//		for (String log : logs) {
//			logList.add(log);
//		}

		ArrayList<Integer> l = new ArrayList<Integer>();

		ArrayList<Integer> l2 = new ArrayList<Integer>();

		Integer hours = null;
		
		HashMap<String, Integer> hm = new HashMap<String, Integer>();

		int counter = 0;
		
		 ArrayList<Integer> listOfIntegers = new ArrayList<Integer>();

		for (int i = 1; i < 23; i++) {

			startDt = LocalDateTime.of(2012, Month.SEPTEMBER, 13, i, 16, 00);

			// System.out.println("new -->" + startDt);

			startDateC = java.sql.Timestamp.valueOf(startDt);
			// System.out.println("old -->" + startDateC.getTime());

			endDt = startDt.plus(1, ChronoUnit.HOURS);
			// System.out.println("new endDt-->" + endDt);

			eC = java.sql.Timestamp.valueOf(endDt);
			// System.out.println("old -->" + eC.getTime());

			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			
			/*
			 * LocalDateTime localDateGood = LocalDateTime.parse( LocalDateTime.now() ,
			 * formatter ); System.out.println( "localDateGood: " + localDateGood );
			 */

			Pattern pattern = Pattern.compile("^(((0[13578]|(10|12))/(0[1-9]|[1-2][0-9]|3[0-1]))|(02/(0[1-9]|[1-2][0-9]))|((0[469]|11)/(0[1-9]|[1-2][0-9]|30)))/[0-9]{4}$");

			Matcher matcher = null;
			String logString = null;
			Date date = null;
			
				
			

			for (int j = 0; j < logList.size(); j++) {
				logString = logList.get(j);
				matcher = pattern.matcher(logString);

				if (matcher.find()) {
					try {
						date = format.parse(matcher.group());

						// formatter.parse
						if (date.getTime() >= startDateC.getTime() && date.getTime() <= eC.getTime()) {
							// System.out.println(logString);
							counter++;
							hours = eC.getHours();

							hm.put("hours", hours);
							l.add(counter);
							l2.add(hours);
						}

					} catch (ParseException e) {
						e.printStackTrace();
					}
				}

			}//
			
			 Optional<Integer> optionalTypeDirName = Optional.ofNullable(hm.get("hours"));
			 
			 listOfIntegers.add(counter);
			 
			// System.out.println(listOfIntegers+"<---");
			 
			 
				
				  if (optionalTypeDirName.isPresent()) { System.out.println("hour recorded :" +
				  hm.get("hours")); }
				 
			
		}
		Integer max = listOfIntegers
			      .stream()
			      .mapToInt(v -> v)
			      .max().orElseThrow(NoSuchElementException::new);
		 
		 System.out.println("Maximum Number of Visitors "+max + " and the Logged hours : "+hm.get("hours"));
		//l.forEach((Integer value) -> System.out.print(value));

	}

}
