package com.student.dto;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class LoadBalancer {

	public static void main(String[] args) throws IOException, ParseException {

		// public static void main(String[] args) {
		String[] logs = new String[] {

				"2012-09-13 16:04:22 DEBUG VISITID:34523 BID:1329 RID:65d33 'Starting new session'",
				"2012-09-13 12:04:30 DEBUG VISITID:34823 BID:1329 RID:54f22 'Authenticating User'",
				"2012-09-13 16:05:30 DEBUG VISITID:42101 BID:319 RID:65a23 'Valid Users'",
				"2012-09-13 16:04:50 ERROR VISITID:34583 BID:1329 RID:54ff3 'Missing Authentication token'",
				"2012-09-26 16:05:31 DEBUG VISITID:42171 BID:319 RID:86472 'Granted Access to  User'",
				"2012-09-13 16:05:31 DEBUG VISITID:42118 BID:319 RID:7a323 'Deleting asset with ID 543234'",
				"2017-09-13 16:05:31 DEBUG VISITID:42011 BID:319 RID:7a323 'Updating asset with ID 543234'",
				"2012-09-13 15:04:50 ERROR VISITID:34623 BID:1329 RID:54ff3 'Missing Authentication token'",
				"2012-09-13 16:05:32 WARN  VISITID:42118 BID:319 RID:7a323 'Invalid asset ID'" };

		
       String fileName = "C:\\Users\\ashrafali.malagi\\Desktop\\Training Details\\Java8\\Logs\\Logger.txt";
		
		String[] logs1 =null;
		
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

		logs1 = stream.toArray(String[]::new);
			
		} catch (IOException e) {
			
		}

		List<String> logList = new ArrayList<String>();
		LocalDateTime startDate = null;
		Date startDateConverted = null;
		LocalDateTime endDate = null;

		Date endDateConverted = null;
		
		Arrays.stream(logs1).forEach(string -> logList.add(string));
		Arrays.stream(logs).forEach(string -> logList.add(string));
		
		
		ArrayList<Integer> listCounter = new ArrayList<Integer>();

		ArrayList<Integer> listHours = new ArrayList<Integer>();

		Integer hours = null;
		
		HashMap<String, Integer> hm = new HashMap<String, Integer>();

		int counter = 0;
		
		 ArrayList<Integer> listOfIntegers = new ArrayList<Integer>();

		for (int i = 1; i < 23; i++) {

			startDate = LocalDateTime.of(2012, Month.SEPTEMBER, 13, i, 16, 00);


			startDateConverted = java.sql.Timestamp.valueOf(startDate);

			endDate = startDate.plus(1, ChronoUnit.HOURS);

			endDateConverted = java.sql.Timestamp.valueOf(endDate);

			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

			Pattern pattern = Pattern.compile("^(\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2})");

			Matcher matcher = null;
			String logString = null;
			Date date = null;

			for (int j = 0; j < logList.size(); j++) {
				logString = logList.get(j);
				matcher = pattern.matcher(logString);

				if (matcher.find()) {
					try {
						date = format.parse(matcher.group());

						// formatter.parse
						if (date.getTime() >= startDateConverted.getTime() && date.getTime() <= endDateConverted.getTime()) {
							// System.out.println(logString);
							counter++;
							hours = endDateConverted.getHours();

							hm.put("hours", hours);
							listCounter.add(counter);
							listHours.add(hours);
						}

					} catch (ParseException e) {
						e.printStackTrace();
					}
				}

			}//
			
			 Optional<Integer> optionalTypeDirName = Optional.ofNullable(hm.get("hours"));
			 
			 listOfIntegers.add(counter);
			 
				  if (optionalTypeDirName.isPresent()) { System.out.println("hour recorded :" + hm.get("hours")); }
				 
			
		}
		Integer max = listOfIntegers
			      .stream()
			      .mapToInt(v -> v)
			      .max().orElseThrow(NoSuchElementException::new);
		 
		 System.out.println("Maximum Number of Visitors "+max + " and the Logged hours : "+hm.get("hours"));

	}

}
