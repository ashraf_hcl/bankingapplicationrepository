package com.bankapplication.service;

import java.util.concurrent.ThreadLocalRandom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bankapplication.entity.AccountDetails;
import com.bankapplication.entity.User;
import com.bankapplication.entity.UserDetails;
import com.bankapplication.repository.UserRepository;
import com.bankapplication.utils.TimeStamps;


@Service
public class UserService {

	@Autowired
	UserRepository userRepository;

	TimeStamps timeStamp = new TimeStamps();

	public void addUserDetails(UserDetails user) {

		User userInformation = new User();

		userInformation.setFirstName(user.getFirstName());
		userInformation.setLastName(user.getLastName());
		userInformation.setMobileNumber(user.getMobileNumber());

		AccountDetails accountDetails = new AccountDetails();
		int accountNumber = Math.abs(ThreadLocalRandom.current().nextInt());
		accountDetails.setAccountNumber(String.valueOf(accountNumber));
		accountDetails.setOpeningBalance("5000");
		accountDetails.setAccountType("Current");
		accountDetails.setCurrentBalance(5000l);

		accountDetails.setTimestamp(timeStamp.getTimeStamp().toString());

		userInformation.setAccountDetails(accountDetails);

		userRepository.save(userInformation);
	}

}
