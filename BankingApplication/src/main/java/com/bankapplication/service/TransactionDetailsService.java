package com.bankapplication.service;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bankapplication.entity.Transaction;
import com.bankapplication.repository.TransactionRepository;


@Service
@Transactional
public class TransactionDetailsService {
	
	
	@Autowired
	TransactionRepository transactionRepository;
	
	public void addTransaction(String fromAccount, String toAccount, Long amount) {


		Transaction transaction = new Transaction();
		transaction.setAmount(amount);

		transaction.setFromAccountNumber(fromAccount);

		transaction.setToAccountNumber(toAccount);

		LocalDateTime dateTime = LocalDateTime.now();
		transaction.setDateTime(dateTime);
		transaction.setDescription(	"Debited from Account : " + fromAccount + " and Credited to Account : " + toAccount);

		transactionRepository.save(transaction);

	}
	
	
}
