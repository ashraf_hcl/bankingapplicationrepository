package com.bankapplication.service;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import com.bankapplication.entity.Transaction;
import com.bankapplication.repository.TransactionRepository;
import com.bankapplication.utils.FormatterDate;


@Service
public class TransactionService {

	
	@Autowired
	TransactionRepository transactionRepository;


	public void addTransactionDetails(Transaction transaction) {

		transactionRepository.save(transaction);
	}
	
	public List<Transaction> fetchBankStatement(String month, String accountNumberReceived, String year) {

		String userEnteredMonth = month;
		int receivedYear = Integer.parseInt(year);
		long accountNumber = Long.valueOf(accountNumberReceived);
		List<Transaction> statements = new ArrayList<Transaction>();
		
		
		List<Transaction> accountExists = transactionRepository.searchAccountNumber(accountNumberReceived);

		if (accountExists.size() == 0) {

			Transaction transaction = new Transaction();
			transaction.setDescription("Account Number Does not exists :");
			statements.add(transaction);
			
			return statements;
		}
		 
		 int monthConverted = FormatterDate.getMonthNumber(userEnteredMonth);
		 Month.valueOf(userEnteredMonth.toUpperCase()).getValue();

		

		if (Month.valueOf(userEnteredMonth.toUpperCase()).getValue() == 9 && year.equalsIgnoreCase("2020")) {

			
			  statements = transactionRepository.searchStats(String.valueOf((Month.valueOf(userEnteredMonth.toUpperCase()).
			  getValue())));
			 
		}

		/*
		 * String dateStartMonth = FormatterDate.getMonthFirstDay(receivedYear,
		 * monthConverted);
		 * 
		 * String userDate = FormatterDate.getMonthLastDaysss(receivedYear,
		 * monthConverted);
		 * 
		 * LocalDate startDate = LocalDate.parse(dateStartMonth);
		 * 
		 * LocalDate endDate = LocalDate.parse(userDate);
		 * 
		 * List<Transaction> detailtransaction =
		 * transactionRepository.searchAccountNumber(String.valueOf(accountNumber));
		 * 
		 * // List<Transaction> listOfData = new ArrayList<Transaction>();
		 * 
		 * 
		 * for (Transaction transaction : detailtransaction) {
		 * 
		 * if (transaction.getDateTime().isAfter(startDate) &&
		 * transaction.getDateTime().isBefore(endDate)) {
		 * 
		 * Transaction ts = new Transaction();
		 * 
		 * BeanUtils.copyProperties(transaction, ts);
		 * 
		 * listOfData.add(ts);
		 * 
		 * } }
		 */
		 

		return statements;

	}


	
	

}
