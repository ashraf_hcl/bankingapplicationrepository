package com.bankapplication.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bankapplication.entity.AccountDetails;
import com.bankapplication.repository.AccountRepository;
import com.bankapplication.utils.TimeStamps;


@Service
public class AccountService {

	@Autowired
	AccountRepository accountRepository;
	
	@Autowired
	TransactionDetailsService transactionDetailsService;
	
	TimeStamps ts = new TimeStamps();
	
	

	public void addAccountDetails(AccountDetails accountDetails) {
		
		accountDetails.setTimestamp(ts.getTimeStamp().toString());
		accountRepository.save(accountDetails);
	}

	public void fundTransferAccountNumber(String fromAccount,String toAccount, Long amountReceived ) {
		
		List<AccountDetails> accountDetails = accountRepository.searchAccountNumber(fromAccount);
		
		Long balance =0l;
		Long amount = amountReceived;
		if(accountDetails.get(0)!=null) {
			balance = accountDetails.get(0).getCurrentBalance();
		}
		
		Long latestBalance = balance - amount;
		int count = 0;
		for (AccountDetails accDetails : accountDetails) {
			accountDetails.get(count).setCurrentBalance(latestBalance);
			
			accDetails.setTimestamp(ts.getTimeStamp().toString());
			accountRepository.save(accDetails);
			count++;
		}


		int counter = 0;
		List<AccountDetails> updateBalance = accountRepository.searchAccountNumber(toAccount);

		for (AccountDetails accDetails : updateBalance) {

			if (updateBalance.get(counter) != null)
				amount = amount + updateBalance.get(counter).getCurrentBalance();

			updateBalance.get(counter).setCurrentBalance(amount);
			accountRepository.save(accDetails);
		}
		
		transactionDetailsService.addTransaction(fromAccount, toAccount, amountReceived);
		
		//return accountRepository.searchAccountNumber(fromAccount);
		

	}

}
