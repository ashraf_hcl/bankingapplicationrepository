package com.bankapplication.dto;

public class UserRegistrationResponse {

	private String status;

	private String statusCode;

	public String getStatus() {
		return status;
	}

	public UserRegistrationResponse() {
		super();
	}

	public UserRegistrationResponse(String status, String statusCode) {
		super();
		this.status = status;
		this.statusCode = statusCode;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

}
