package com.bankapplication.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.boot.autoconfigure.domain.EntityScan;

@Entity
@EntityScan(basePackages = {"com.example.demo.entity"})
@Table(name="user")
public class User {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	// @Min(3)
	// @Max(8)
	@NotNull(message = "First Name is required")
	@Column(name = "first_name")
	private String firstName;

	@NotNull(message = "Last Name is required")
	@Column(name = "last_name")
	private String lastName;

	// remove

	@OneToOne(cascade = CascadeType.ALL)

	@JoinColumn(name = "id", referencedColumnName = "id")
	private AccountDetails accountDetails;

	public AccountDetails getAccountDetails() {
		return accountDetails;
	}

	public void setAccountDetails(AccountDetails accountDetails) {
		this.accountDetails = accountDetails;
	}

	@NotNull(message = "Mobile Number is required")
	@Column(name = "mobile_number", unique = true, length = 10)

	private String mobileNumber;

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

}
