package com.bankapplication.utils;

import java.sql.Timestamp;
import java.util.Date;

public class TimeStamps {
	
	
	public Timestamp getTimeStamp(){
		
		Date date = new Date();
		long time = date.getTime();
		Timestamp timestamp = new Timestamp(time);
		
		return timestamp;
	}

}
