package com.bankapplication.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;

public class FormatterDate {


    public static LocalDateTime stringToDate(String str, String dateFormate) {


        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormate);
        LocalDateTime dateTime = LocalDateTime.parse(str, formatter);


        return dateTime;
    }






    // getting month start date and end date based on year and month
    public static String getMonthFirstDay(int year, int month) {


        YearMonth yearMonth = YearMonth.of(year, month);
        LocalDate fromDateFirstDayOfMonth = yearMonth.atDay(1);
        String fromDateData = fromDateFirstDayOfMonth.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        return fromDateData;
    }






    public static int getMonthNumber(String monthName) {
        return Month.valueOf(monthName.toUpperCase()).getValue();
    }


    public static int getMonthDaysCount(int year, int month) {


        YearMonth yearMonthObject = YearMonth.of(year, month);
        int daysInMonth = yearMonthObject.lengthOfMonth();
        return daysInMonth;
    }


    public static String getMonthLastDaysss(int year, int month) {


        YearMonth yearMonth = YearMonth.of(year, month);
        LocalDate last = yearMonth.atEndOfMonth();
        String toDateData = last.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        return toDateData;
    }
}