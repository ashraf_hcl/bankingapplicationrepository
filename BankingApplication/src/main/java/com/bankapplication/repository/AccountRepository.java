package com.bankapplication.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bankapplication.entity.AccountDetails;

@Repository
public interface AccountRepository extends JpaRepository<AccountDetails, Long> {

	@Query(value = "select * from accountdetails u where u.account_number = :accountNumber", nativeQuery = true)
	public List<AccountDetails> searchAccountNumber(@Param("accountNumber") String accountNumber);

}
