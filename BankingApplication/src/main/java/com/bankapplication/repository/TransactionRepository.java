package com.bankapplication.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bankapplication.entity.AccountDetails;
import com.bankapplication.entity.Transaction;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

	@Query(value = "select * from transaction u where u.from_account_number = :accountNumber", nativeQuery = true)
	public List<Transaction> searchAccountNumber(@Param("accountNumber") String accountNumber);

	@Query(value = "SELECT * FROM transaction WHERE month(date_time) = :stats", nativeQuery = true)
	public List<Transaction> searchStats(@Param("stats") String stats);


}
