package com.bankapplication.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bankapplication.dto.UserRegistrationResponse;
import com.bankapplication.entity.UserDetails;
import com.bankapplication.service.UserService;


@RestController
@RequestMapping("/bank")
public class UserController {

	@Autowired
	UserService userService;


	@PostMapping("/user/registration")
	public UserRegistrationResponse userRegister(@Valid @RequestBody UserDetails user) {
		UserRegistrationResponse response = new UserRegistrationResponse();

		try {

			userService.addUserDetails(user);

			response.setStatus("User registered successfully");

			response.setStatusCode("200");

		} catch (Exception e) {
		}

		return response;
	}


}