package com.bankapplication.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bankapplication.entity.Transaction;
import com.bankapplication.service.TransactionService;


@RestController
@RequestMapping("/bank/transaction")
public class TransactionController {

	@Autowired
	TransactionService transactionService;

	@GetMapping("/statements")
	public List<Transaction> fetchBankStatement(@RequestParam(required = true) String month,
			@RequestParam(required = true) String accountNumber, @RequestParam(required = true) String year) {

		return transactionService.fetchBankStatement(month, accountNumber, year);
	}

}