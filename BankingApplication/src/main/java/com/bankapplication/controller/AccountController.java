package com.bankapplication.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bankapplication.dto.UserRegistrationResponse;
import com.bankapplication.entity.AccountDetails;
import com.bankapplication.entity.Transaction;
import com.bankapplication.service.AccountService;
import com.bankapplication.service.TransactionService;


@RestController
@RequestMapping("/bank")
public class AccountController {

	@Autowired
	AccountService accountService;

	@Autowired
	TransactionService transactionService;

	
	@PostMapping("/fundtransfer")
	public UserRegistrationResponse fundTransfer(@RequestParam(required = true) String fromAccount,
			@RequestParam String toAccount, @RequestParam Long amount) {

		UserRegistrationResponse response = new UserRegistrationResponse();
		try {

			 accountService.fundTransferAccountNumber(fromAccount, toAccount, amount );
			

			response.setStatus("Fund Transferred Successfully");

			response.setStatusCode("200");

		} catch (Exception e) {

		}

		return response;
	}



}